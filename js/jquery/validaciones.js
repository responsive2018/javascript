/**
*   VALIDACION PARA SOLO NUMEROS
*/
$('.soloNumeros').on('input propertychange', function(){
    var RegExPattern = /^\d+$/;
	$(this).val($(this).val().replace(/[[a-zA-z\u00D1\u00F1\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC@. ]/gi, ""));
});
/**
*   VALIDACION PARA SOLO ALPHA
*/
$('.soloAlpha').on('input propertychange',function(){
    var RegExPattern = /[a-zA-z\u00B4\u00D1\u00F1\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC@. ]$/;
    if (!$(this).val().match(RegExPattern)) {
        $(this).val($(this).val().substring(0,$(this).val().length-1));
    }  
});
/**
*   VALIDACION PARA SOLO ALFANUMERICOS DE CORREO
*/
$('.soloAlphaCorreo').on('input propertychange',function(){
    var RegExPattern = /[a-zA-z0-9\u00D1\u00F1\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC@. ]$/;
    if (!$(this).val().match(RegExPattern)) {
        $(this).val($(this).val().substring(0,$(this).val().length-1));
    }  
});