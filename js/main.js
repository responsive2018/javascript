// // variables
// var nombreMiVaiable = "";

// // funciones
// imprimeHola();
// function imprimeHola() 
// {
// 	console.log("function - Hola Mundo\n\n");
// }

// imprimeNombre();
// function imprimeNombre( nombre = "-" ) 
// {
// 	// console.log("Tu nombre es: \"" + nombre + "\"");
// 	console.log( `Tu nombre es: "${nombre}"` ); //type script
// }

// // condicionales
// console.log("==== Condicionales ====\n");
// var flag = false; // variable de bandera para ...
// var dataToSendToSomething = "";
// if ( flag ) {
// 	console.log("la bandera esta activa", flag);
// } else {
// 	console.log("la bandera esta deactivada", flag);
// }

// var condicional = ( flag == true ) ? "si es true" : "no lo es";
// console.log(condicional);

// // arreglos
// console.log("\n\n==== Arreglos ====\n");
// var arreglo = new Array();
// arreglo.push(1); // posicion 0
// arreglo.push("Hola"); // pos 1
// arreglo.push("Mundo"); // pos 2

// var arreglo2 = new Array();
// arreglo2[0] = "Hola";
// arreglo2[1] = "Mundo";
// arreglo2[2] = "XD";

// var arreglo3 = [3, "Hola", "Mundo", "XD XD XD"];

// var matriz = [["mat11", "mat12", "mat13"],
// 			  ["mat21", "mat22", "mat23"]];

// console.log( matriz[1][2] );

// console.log("Arreglos asociativos");
var usuarios 							= new Object();
usuarios["id"] 							= 1;
usuarios["name"] 						= "nombre1";
usuarios["nameasdsa"] 					= "nombre1";
usuarios["nameasasd"] 					= "nombre1";
usuarios["namasdasde"] 					= "nombre1";
usuarios["namasdkashgdjhasgdjasgdhjge"] = "nombre1";
usuarios["name"] 						= "nombre1";

/**
* Metodo para kajshgdjhas
* @param int: edad
* @return json : respuesta de la validacion
*/
function code()
{
	// 
}
// var usuarios = [{"id": 1, "name": "nombre1", "edad": 30, "times": [1,2,3,4]},
// 				{"id": 2, "name": "nombre2"}];
// console.log(usuarios["name"]);
// console.log(usuarios.name);

// console.log( arreglo );
// console.log( arreglo2 );
// console.log( arreglo3 );

// // ciclos
// console.log("\n\n==== Ciclos ====\n");
// var arr = [1,2,3,4,5,6,7,8];
// var usuarios = [{"id": 1, "name": "nombre1"},
// 			{"id": 2, "name": "nombre2"},
// 			{"id": 3, "name": "nombre3"},
// 			{"id": 4, "name": "nombre4"}
// 			];

// console.log("\n\n== WHILE ==\n");
// var cont = 0;
// // var total = arr.length;
// while( typeof arr[cont] != "undefined" ) {
// 	// while( cont < total ) {
// 	console.log( arr[ cont ] );
// 	cont++;
// }

// var cont = 0;
// while( typeof usuarios[cont] != "undefined" ) {
// 	console.log( usuarios[ cont ] );
// 	cont++;
// }

// console.log("\n\n== FOR ==\n");

// for( var c = 0; c < arr.length; c++ ) {
// 	console.log( arr[ c ]);
// }

// for( var c = 0; c < usuarios.length; c++ ) {
// 	console.log( usuarios[ c ]);
// }

// console.log("\n\n== FOR IN ==\n");
// // for in - se usa mas para los objetos
// // por ejemplo tenemos un arreglo de base de datos que se llama usuarios y queda así:
// for ( usuario in usuarios ) {
// 	console.log(usuarios[usuario]);
// }


// // strings
// console.log("\n\n==== Strings (Cadenas) ====\n");
// var texto = "Este es un texto simulado";

// console.log("\n\n== split ==\n");
// console.log( texto.split(" ") );


// console.log("\n\n== join ==\n");
// // no hay soporte para IE 11 ni anteriores solo EDGE
// var separados = texto.split(" ");
// console.log( separados.join("/") );

// console.log("\n\n== includes ==\n");
// // no hay soporte para IE 11 ni anteriores solo EDGE
// var separados = texto.split(" ");
// console.log( separados.includes("texto") );

// console.log("\n\n== toUppserCase ==\n");
// // no hay soporte para IE 11 ni anteriores solo EDGE
// console.log( texto.toUpperCase() );


// console.log("\n\n== toLowerCase ==\n");
// // no hay soporte para IE 11 ni anteriores solo EDGE
// console.log( texto.toLowerCase() );


// console.log("\n\n== length ==\n");
// console.log( texto.length );

// // objeto asociativo
// console.log("\n\n== Arreglo asociativo ==\n");
// var clientes 		= new Object();
// clientes["id"] 		= 1;
// clientes["nombre"] 	= "nombre de cliente 1";

// var usuarios = {"id": 1, "name": "nombre1"};
// console.log( clientes, usuarios );



console.log("\n\n== Funciones de tiempo ==\n");
var today 	= new Date();
// // var dd 		= today.getDate();
// // var mm 		= today.getMonth() + 1; //January is 0!
// // var yyyy 	= today.getFullYear();
var hh 		= today.getHours();
var min 	= today.getMinutes();
var sec 	= today.getSeconds();
console.log(`${hh}:${min}:${sec}`);
console.log("Ejecucion de funcion despues de 5 seg - setTimeout");

setTimeout(function(){
	// var today 	= new Date();
	// var hh 		= today.getHours();
	// var min 	= today.getMinutes();
	// var sec 	= today.getSeconds();
	// console.log(`Se ejecuta funcion: ${hh}:${min}:${sec}`);

	// console.log("Ejecucion de funcion cada segundo - setInterval");
	// setInterval(function(){
	// 	var today 	= new Date();
	// 	var hh 		= today.getHours();
	// 	var min 	= today.getMinutes();
	// 	var sec 	= today.getSeconds();
	// 	console.log(`La hora Actual: ${hh}:${min}:${sec}`);
	// }, 1000);
}, 1000);


console.log("\n\n== Métodos Matemáticos Ceil, Floor ==\n");
var total = 10;
var division = 3;
// normal
console.log("Division normal (10/3): " + total / division);

// ceil
console.log("Division ceil: " +  Math.ceil( total/division ) );

// floor
console.log("Division floor: " +  Math.floor( total/division ) );


console.log("\n\n== Manejo de errores ==\n");
try {
	enviarInformacion();
} catch(err) {
	console.log(err);
}
console.log("nuestro programa no muere con el error");



console.log("\n\n== Callback ==\n");
// hacemos la creacion de una function normal donde sabemos que param 2 es una funcion anonima
function llamada(nombre, param2) {
	setTimeout(function(){
		console.log(nombre);
		param2();
	},1000);
}

llamada("Mario", function(){
	alert("Se hizo el callback");
});


console.log("\n\n== Metodos recursivos ==\n");
// hacemos el factorial de un numero
var numero = 3; //su factorial es 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1;

function factorial( numero )  {
	if ( numero > 1 ) {
		return numero * factorial( numero - 1 );
	} else {
		return 1;
	}
}

console.log( `El factorial de ${numero} es ${factorial(numero)}`);














