<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

// se hace la recepcion de datos y hacemos que se esperen en contestar 5 seg
if ( count( $_GET ) > 0 ) {
	// sabemos que los datos llegan por GET
	$datos = $_GET;
	$datos["tipo"] = "GET";	
} else {
	$datos = $_POST;
	$datos["tipo"] = "POST";	
}

sleep( 5 );
echo json_encode($datos);