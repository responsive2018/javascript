# Javascript

### Temario

* Variables
* Funciones
* Condicionales
* Ciclos
* Strings
  - split
  - includes
  - join
  - toUpperCase
  - toLowerCase
  - length
* Arreglos asociativos
* Métodos de control de tiempo de ejecución
  - setTimeout
  - setInterval
- Métodos matemáticos básicos
  - ceil
  - floor
- Manejo de errores
  - try {} catch(err){}
- typeof undefined
- callback
- Métodos recursivos


### jQuery


Recursos
Link [jQuery descarga](https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js)
directo https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js

- ¿Que es jQuery?
- Método function
- Modificar DOM HTML
- Obtener informacion de DOM
- Listeners
- $.each();
- fadeIn
- fadeOut
- hide
- show
- closest
- children
- append
- preppend
- remove
- addClass
- removeClass
- animate
- Calculos de pantalla
  - Alto
  - Ancho
- style
- attr
- AJAX
  - $.ajax
  - $.post
  - $.get


Especiales:

* Link [Formato HTML](https://www.freeformatter.com/)
* Link [Formato JS](http://jsbeautifier.org/)
* Link [Para TyC y Avsiso de privacidad](https://www.tinymce.com/docs/demo/full-featured/)
